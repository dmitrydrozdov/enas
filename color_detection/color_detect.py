import cv2
import numpy as np
import time
import socket

class Vision:
    def getGreenMask(self,hsv1):
        green = cv2.inRange(hsv1,np.array((40,100,20)),np.array((80,255,255)))
        return green
    
    def getRedMask(self,hsv):
        red = cv2.inRange(hsv,np.array((0,100,50)),np.array((10,255,255)))
        red1 = cv2.inRange(hsv,np.array((170,150,100)),np.array((180,255,255)))
        red = cv2.add(red,red1)	
        return red

    def __init__(self, cameraIndex):
        self.camera = cv2.VideoCapture(cameraIndex)
        #self.camera.set(3,480)
        #self.camera.set(4,320)

    def detect(self):
        result = []

        #read 1 image:
		#Release the buffer
        self.camera.grab()
        self.camera.retrieve()
        self.camera.grab()
        self.camera.retrieve()
        self.camera.grab()
        _, frame = self.camera.retrieve()
        frame = frame[0:480,0:480]
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        #cv2.imshow('frame',hsv)
        #cv2.waitKey()
        
        #red color
        redMask = self.getRedMask(hsv)
        
        redMask = cv2.erode(redMask, None, iterations=2)
        redMask = cv2.dilate(redMask, None, iterations=2)
        output1 = cv2.bitwise_and(hsv, hsv, mask = redMask)
        #cv2.imshow("Red", output1)
        #cv2.waitKey()
        histrRed = cv2.calcHist([redMask],[0],None,[2],[0,1])
        print "histoRed"
        print histrRed[0]
        if(histrRed[0] < 190000):
            print "Detect color Red"
            result.append(True)
        else:
            result.append(False)

        #green color
        greenMask = self.getGreenMask(hsv)
        greenMask = cv2.erode(greenMask, None, iterations=2)
        greenMask = cv2.dilate(greenMask, None, iterations=2)
        output2 = cv2.bitwise_and(hsv, hsv, mask = greenMask)
        #cv2.imshow("Green", output2)
        #cv2.waitKey()
        histGreen = cv2.calcHist([greenMask],[0],None,[2],[0,1])
        print "histGreen"
        print histGreen[0]
        if(histGreen[0] < 190000):
            print "Detect color Green"
            result.append(True)
        else:
            result.append(False)

        return result



UDP_IP = "192.168.0.178"
UDP_PORT = 9013
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
sock.bind((UDP_IP, UDP_PORT))
vision = Vision(-1)

while(1):
    try:
        data, address = sock.recvfrom(1024) # buffer size is 1024 bytes
        if (data == "CHECK"):
            print "Request for quality check"
            time.sleep(1)
            vision.camera.retrieve()
            result = vision.detect()
            print result
            if result[0] == True:
                sock.sendto("RED", address) 
            elif result[1] == True:
                sock.sendto("GREEN", address)
            else:
                sock.sendto("EMPTY", address)   
	    #vision.detect()        
	#vision.camera.retrieve()
    except KeyboardInterrupt:
        pass

sock.shutdown()
sock.close()
vision.camera.release()
cv2.destroyAllWindows()
