#ifndef _UDPtoNxt_H_
#define _UDPtoNxt_H_

#include "plugin/gui/UnitDialog.h"

class UDPtoNxt : public plugin::gui::UnitDialog
{
	Q_OBJECT

public:
	UDPtoNxt (plugin::gui::UnitDelegate& del);

	void update (const plugin::gui::UnitHistoryBundle& buf);
	void translate (void);

private:
};

#endif // _UDPtoNxt_H_
