#include "UDPtoNxt.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include<stdio.h>
#include<winsock2.h>


#pragma comment(lib,"ws2_32.lib")

#define SERVER "192.168.0.34"
//#define SERVER "127.0.0.1"
#define PORT 9180
#define BUFLEN 40 
#define TIMEOUT 20

struct sockaddr_in si_other;
int s, slen = sizeof(si_other);
char buf[BUFLEN];
char message[BUFLEN];
char oldmessage[BUFLEN];
WSADATA wsa;

UDPtoNxt::UDPtoNxt (plugin::simulation::UnitDelegate& del)
: plugin::simulation::Unit(del, plugin::simulation::Deterministic) {
	
	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}
	printf("Initialised.\n");

	//create socket
	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		printf("socket() failed with error code : %d", WSAGetLastError());
		exit(EXIT_FAILURE);
	}

	static int timeout = TIMEOUT;
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));
	setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));

	//setup address structure
	memset((char *)&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(PORT);
	si_other.sin_addr.S_un.S_addr = inet_addr(SERVER);

	xout = 0;
	yout = 0;
	poseout	= 0;
	dockout = 0;
	out0 = 0;
	out1 = 0;
	out2 = 0;
	out3 = 0;
	out4 = 0;
	out5 = 0;
	out6 = 0;	
}

UDPtoNxt::~UDPtoNxt() {
	closesocket(s);
	WSACleanup();
}

void UDPtoNxt::step (void) {

	memset(message, NULL, BUFLEN);
	
	int send_data = readInput("send_data").toBool();
	int xin = readInput("xin").toInt();
	int yin = readInput("yin").toInt();
	int posein = readInput("posein").toInt();
	int dockin = readInput("dockin").toInt();
	bool in0 = readInput("in0").toBool();
	bool in1 = readInput("in1").toBool();
	bool in2 = readInput("in2").toBool();
	bool in3 = readInput("in3").toBool();
	bool in4 = readInput("in4").toBool();
	bool in5 = readInput("in5").toBool();
	bool in6 = readInput("in6").toBool();
	char docking_str[4];

	if (dockin == 1) {
		docking_str[0] = 'C';
		docking_str[1] = 'O';
		docking_str[2] = 'L';
		docking_str[3] = 'L';
	}
	else if (dockin == 2) {
		docking_str[0] = 'F';
		docking_str[1] = 'E';
		docking_str[2] = 'E';
		docking_str[3] = 'D';
	}
	else if (dockin == 3) {
		docking_str[0] = 'H';
		docking_str[1] = 'O';
		docking_str[2] = 'M';
		docking_str[3] = 'E';
	}
	else {
		docking_str[0] = '0';
		docking_str[1] = ' ';
		docking_str[2] = ' ';
		docking_str[3] = ' ';
	}

	int ret = sprintf(message, "%6d", xin);
	ret = ret + sprintf(message + ret, "%1c", ' ');
	ret = ret + sprintf(message + ret, "%6d", yin);
	ret = ret + sprintf(message + ret, "%1c", ' ');
	ret = ret + sprintf(message + ret, "%6d", posein);
	ret = ret + sprintf(message + ret, "%1c", ' ');
	//ret = ret + sprintf(message + ret, "%4d", dockin);
	ret = ret + sprintf(message + ret, "%1c", docking_str[0]);
	ret = ret + sprintf(message + ret, "%1c", docking_str[1]);
	ret = ret + sprintf(message + ret, "%1c", docking_str[2]);
	ret = ret + sprintf(message + ret, "%1c", docking_str[3]);
	ret = ret + sprintf(message + ret, "%1c", ' ');
	ret = ret + sprintf(message + ret, "%1d", in0);
	ret = ret + sprintf(message + ret, "%1d", in1);
	ret = ret + sprintf(message + ret, "%1d", in2);
	ret = ret + sprintf(message + ret, "%1d", in3);
	ret = ret + sprintf(message + ret, "%1d", in4);
	ret = ret + sprintf(message + ret, "%1d", in5);
	ret = ret + sprintf(message + ret, "%1d", in6);
	
	if (send_data == TRUE && strcmp(oldmessage, message) != 0) {
		strncpy(oldmessage, message, 40);
		//send the message
		if (sendto(s, message, strlen(message), 0, (struct sockaddr *) &si_other, slen) == SOCKET_ERROR) {
			printf("sendto() failed with error code : %d", WSAGetLastError());
		}
	}
	
	memset(buf, NULL, BUFLEN);
	
	//receive data
	if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen) != SOCKET_ERROR)
	{
		// run only if socket does not timeout
		sscanf(buf, "%6d %6d %6d %4s %1d %1d %1d %1d %1d %1d %1d", &xout, &yout, &poseout, &dockout_str,
			&out0, &out1, &out2, &out3, &out4, &out5, &out6);
	}
	
	if (strcmp(dockout_str, "COLL") == 0) {
		dockout = 1;
	}
	else if (strcmp(dockout_str, "FEED") == 0) {
		dockout = 2;
	}
	else if (strcmp(dockout_str, "HOME") == 0) {
		dockout = 3;
	}
	else {
		dockout = 0;
	}

	writeOutput("xout", xout);
	writeOutput("yout", yout);
	writeOutput("poseout", poseout);
	writeOutput("dockout", dockout);
	writeOutput("out0", out0);
	writeOutput("out1", out1);  
	writeOutput("out2", out2);
	writeOutput("out3", out3);
	writeOutput("out4", out4);
	writeOutput("out5", out5);
	writeOutput("out6", out6);
}

