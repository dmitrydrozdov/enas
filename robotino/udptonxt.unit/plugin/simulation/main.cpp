#include "plugin/simulation/Interface.h"
#include "UDPtoNxt.h"

BEGIN_SIMULATION_INTERFACE( "blockdesigner", "UDPtoNxt" )
	BEGIN_UNITS
		ADD_UNIT( "blockdesigner UDPtoNxt", UDPtoNxt )
	END_UNITS
END_SIMULATION_INTERFACE
