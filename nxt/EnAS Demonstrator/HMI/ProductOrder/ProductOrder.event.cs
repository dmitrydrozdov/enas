/*
 * Created by nxtSTUDIO.
 * User: dmidro
 * Date: 9/23/2018
 * Time: 2:03 PM
 * 
 */
using System;
using NxtControl.GuiFramework;
using NxtControl.Services;

#region Definitions;
#region #ProductOrder_HMI;

namespace HMI.Main.Symbols.ProductOrder
{

  public class CNFEventArgs : System.EventArgs
  {
    public CNFEventArgs()
    {
    }
    private System.Boolean? reset_field = null;
    public System.Boolean? reset
    {
       get { return reset_field; }
       set { reset_field = value; }
    }
    private System.Int16? numGreen_field = null;
    public System.Int16? numGreen
    {
       get { return numGreen_field; }
       set { numGreen_field = value; }
    }
    private System.Int16? numRed_field = null;
    public System.Int16? numRed
    {
       get { return numRed_field; }
       set { numRed_field = value; }
    }

  }

}

namespace HMI.Main.Symbols.ProductOrder
{
  partial class sDefault
  {
    public bool FireEvent_CNF(System.Boolean reset, System.Int16 numGreen, System.Int16 numRed)
    {
      return ((IHMIAccessorOutput)this).FireEvent(0, new object[] {reset, numGreen, numRed});
    }
    public bool FireEvent_CNF(HMI.Main.Symbols.ProductOrder.CNFEventArgs ea)
    {
      object[] _values_ = new object[3];
      if (ea.reset.HasValue) _values_[0] = ea.reset.Value;
      if (ea.numGreen.HasValue) _values_[1] = ea.numGreen.Value;
      if (ea.numRed.HasValue) _values_[2] = ea.numRed.Value;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }
    public bool FireEvent_CNF(System.Boolean reset, bool ignore_reset, System.Int16 numGreen, bool ignore_numGreen, System.Int16 numRed, bool ignore_numRed)
    {
      object[] _values_ = new object[3];
      if (!ignore_reset) _values_[0] = reset;
      if (!ignore_numGreen) _values_[1] = numGreen;
      if (!ignore_numRed) _values_[2] = numRed;
      return ((IHMIAccessorOutput)this).FireEvent(0, _values_);
    }

  }
}
#endregion #ProductOrder_HMI;

#endregion Definitions;
