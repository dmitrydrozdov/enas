﻿/*
 * Created by nxtSTUDIO.
 * User: mangu
 * Date: 12/7/2016
 * Time: 1:27 PM
 * 
 */
using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;

using NxtControl.GuiFramework;

namespace HMI.Main.Canvases
{
	/// <summary>
	/// Summary description for EnAS.
	/// </summary>
	partial class EnAS
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// EnAS
			// 
			this.Bounds = new NxtControl.Drawing.RectF(((float)(0)), ((float)(0)), ((float)(1024)), ((float)(688)));
			this.Brush = new NxtControl.Drawing.Brush("CanvasBrush");
			this.Name = "EnAS";
			this.Size = new System.Drawing.Size(1024, 688);
		}
		#endregion
	}
}
