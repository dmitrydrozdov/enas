﻿/*
 * Created by nxtSTUDIO.
 * User: Aalto_IT
 * Date: 10/12/2016
 * Time: 10:14 PM
 * 
 */

using System;
using System.Drawing;
using NxtControl.GuiFramework;

namespace HMI.Main.Symbols.StartPauseResetButtons
{
	/// <summary>
	/// Description of sDefault.
	/// </summary>
	public partial class sDefault : NxtControl.GuiFramework.HMISymbol
	{
		public sDefault()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
		}
		
		void StartClick(object sender, EventArgs e)
		{
		  Start.Click += new EventHandler(this.StartClick_off);
		  
		}
		void StartClick_off(object sender, EventArgs e)
		{
		  Start.Checked = true;
		  Reset.Enabled = false;
		  RunningText.Visible = true;
		  PausedText.Visible = false;
		  ResetText.Visible = false;
		}
		
		void PauseClick(object sender, EventArgs e)
		{
		  Pause.Click += new EventHandler(this.PauseClick_off);
		}
		void PauseClick_off(object sender, EventArgs e)
		{
		  Pause.Checked = true;
		  Reset.Enabled = true;
		  RunningText.Visible = false;
		  PausedText.Visible = true;
		  ResetText.Visible = false;
		}
		
		void ResetClick(object sender, EventArgs e)
		{
			Reset.Click += new EventHandler(this.ResetClick_off);
			
		}
	  void ResetClick_off(object sender, EventArgs e)
		{
		  Reset.Checked = true;
		  RunningText.Visible = false;
		  PausedText.Visible = false;
		  ResetText.Visible = true;
		}
		
		
		void StartInValueChanged(object sender, ValueChangedEventArgs e)
		{
		  if((bool)e.Value == true) {
  		  Reset.Enabled = false;
  		  RunningText.Visible = true;
  		  PausedText.Visible = false;
  		  ResetText.Visible = false;
		  }
		}
		
		void PauseInValueChanged(object sender, ValueChangedEventArgs e)
		{
		  if((bool)e.Value == true) {
  			Reset.Enabled = true;
  		  RunningText.Visible = false;
  		  PausedText.Visible = true;
  		  ResetText.Visible = false;
		  }
		}
		
		void ResetInValueChanged(object sender, ValueChangedEventArgs e)
		{
		  if((bool)e.Value == true) {
    		RunningText.Visible = false;
    	  PausedText.Visible = false;
    	  ResetText.Visible = true;
		  }
		}
	}
}
